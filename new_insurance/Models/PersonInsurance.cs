﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace new_insurance.Models
{
    public class PersonInsurance
    {
        [Key]
        [Column(Order = 0)]
        public int PersonInsuranceId { get; set; }

        [Display(Name="Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe introducir su nombre")]
        public String Name { get; set; }

        [Display(Name="Apellidos")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe introducir sus apellidos")]
        public String LastName { get; set; }


        #region Domicilio

        [Display(Name ="Calle")]
        public String Street { get; set; }

        [Display(Name = "No. int.")]
        public String NoInt { get; set; }

        [Display(Name = "No. ex.")]
        public String NoEx { get; set; }

        [Display(Name = "Colonia")]
        public String Colony { get; set; }

        [Display(Name = "Pais")]
        public List<SelectListItem> CountryListItem { get; set; }

        [Display(Name = "Estado")]
        public String State { get; set; }

        [Display(Name = "Ciudad")]
        public String City { get; set; }

        #endregion

        [Display(Name = "Telefono")]
        [DataType(DataType.PhoneNumber)]
        public String PhoneNumber { get; set; }

        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress, ErrorMessage = "La direccion de correo es incorrecta")]
        public String Email { get; set; }

        [Display(Name = "Fecha de nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe introducir la fecha de nacimiento")]
        public DateTime Birthdate { get; set; }

        [Display(Name = "Monto")]
        [DataType(DataType.Currency)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe introducir un monto a solicitar")]
        [Range(1, 50000.0, ErrorMessage ="El monto debe ser entre 1 y 50000")]
        public double Ammount { get; set; }


        [Display(Name = "Riesgo ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Necesitamos conocer el riesgo")]
        public Risk Risk { get; set; }
        
        public static IEnumerable<SelectListItem> GetRiskList()
        {
            yield return new SelectListItem { Value = "", Text = "Riesgo", Selected = true };
            yield return new SelectListItem { Value = "Bajo", Text = "Bajo" };
            yield return new SelectListItem { Value = "Medio", Text = "Medio" };
            yield return new SelectListItem { Value = "Alto", Text = "Alto" };
        }

        [Display(Name = "Observaciones ")]
        [Required(AllowEmptyStrings =false, ErrorMessage ="Debe introducir las observaciones de su solicitud.")]
        [MinLength(50, ErrorMessage = "Debe introducir mas de 50 caracteres")]
        [MaxLength(250, ErrorMessage = "Debe introducir menos de 250 caracteres")]
        public String Observations { get; set; }
    }

    public enum Risk
    {
        Riesgo, Bajo, Medio, Alto
    }
}