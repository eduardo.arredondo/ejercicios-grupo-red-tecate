﻿using System;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;

namespace new_insurance.Models
{
    public class Repository
    {
        private NewDataSet countryRepository;
        private String pathRespository = "~/App_Data/country.xml";

        public string GetPathRespository()
        {
            return pathRespository;
        }

        public void InitializeRepository(String DataPath)
        {
            var str = new StreamReader(DataPath);
            var xSerializer = new XmlSerializer(typeof(NewDataSet), new XmlRootAttribute("plist"));
            countryRepository = (NewDataSet)xSerializer.Deserialize(str);
        }

        public DataRowCollection GetCountrysFromRepository()
        {
            DataTableCollection countrys = GetCountryElementsFromRespository();
            return countrys["Country"].Rows;
        }

        public List<SelectListItem> GetRiskFromRepository()
        {
            List<SelectListItem> RiskList = new List<SelectListItem>();
            RiskList.Add(new SelectListItem() { Value = "1", Text = "Bajo" });
            RiskList.Add(new SelectListItem() { Value = "2", Text = "Medio" });
            RiskList.Add(new SelectListItem() { Value = "3", Text = "Alto" });
            return RiskList;
        }

        public IEnumerable<SelectListItem> GetRiskList()
        {
            yield return new SelectListItem { Value = "1", Text = "Bajo" };
            yield return new SelectListItem { Value = "2", Text = "Medio" };
            yield return new SelectListItem { Value = "3", Text = "Alto" };
        }

        private DataTableCollection GetCountryElementsFromRespository()
        {
            return countryRepository.CountryElement.DataSet.Tables;
        }
    }
}