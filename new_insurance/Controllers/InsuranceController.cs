﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

using new_insurance.Models;

namespace new_insurance.Controllers
{
    public class InsuranceController : Controller
    {
        // GET: Insurance2
        public ActionResult Index(PersonInsurance model)
        {
            int age = DateTimeExtensions.Age(model.Birthdate);

            if (model.Risk.Equals(Risk.Riesgo))
            {
                ModelState.AddModelError("Risk", "Necesitamos conocer el riego");
            }

            //Si es mayor de 65 años la cantidad no puede ser mayor 10,000
            if (age > 65 && model.Ammount > 10000)
            {
                ModelState.AddModelError("Ammount", "El monto no debe exceder los 10000 pesos");
            }

            //Si el riesgo es alto no mayor de 5,000
            if (model.Risk.Equals(Risk.Alto) && model.Ammount > 5000)
            {
                ModelState.AddModelError("Ammount", "El riesgo es Alto, el monto permitido es menor o igual a 5000"); 
            }

            //o	Si es medio y mayor a 50, no mayor a 7,500
            if (model.Risk.Equals(Risk.Medio) && age > 50) {
                if(model.Ammount > 7500)
                {
                    ModelState.AddModelError("Ammount", "El riesgo es Alto, el monto permitido es menor o igual a 7500");
                }
            }

            //Ninguna cantidad debe pasar de 50,000
            if (model.Ammount > 50000)
            {
                ModelState.AddModelError("Ammount", "El monto debe ser menor de 50000");
            }

            var country = model.CountryListItem;

            if(!ModelState.IsValid)
            {
                var modelPerson = BuildModelFromPersonInsurance(model);
                return View("Create", modelPerson);
            }

            return View(model);

        }

        // GET: Insurance/Create
        public ActionResult Create()
        {
            var insurance = new PersonInsurance();
            insurance.Birthdate = DateTime.Now.Date;

            var model = BuildModelFromPersonInsurance(insurance);

            return View(model);
        }

        private List<SelectListItem> BuildCountryListItem(DataRowCollection country)
        {
            List<SelectListItem> countryListItem = new List<SelectListItem>();
            for (int i = 0; i < country.Count - 1; i++)
            {
                countryListItem.Add(new SelectListItem
                {
                    Value = i.ToString(),
                    Text = Convert.ToString(country[i]["CountryName"]),
                });
            }

            return countryListItem;
        }

        private PersonInsurance BuildModelFromPersonInsurance(PersonInsurance model)
        {
            Repository repository = new Repository();
            string PathRespository;

            PathRespository = Server.MapPath(repository.GetPathRespository());
            repository.InitializeRepository(PathRespository);

            model.CountryListItem = BuildCountryListItem(repository.GetCountrysFromRepository());

            return model;
        }
    }
}
